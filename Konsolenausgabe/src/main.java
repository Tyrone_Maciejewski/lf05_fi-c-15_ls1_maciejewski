
public class main {
int bUcHsRE;
int z_;
	public static void main(String[] args) {
		//Aufgabe 1
		System.out.print("Das ist ein Beispielsatz.");
		System.out.print("Ein Beispielsatz ist das.");
		
		System.out.println("Das ist ein \"Beispielsatz\".");
		System.out.print("Ein Beispielsatz ist das.");
		
		/*  print()- und der println()-Anweisung 
		  print(); 
		  	gibt Argumente in der Console wieder ohne einen Zeilen umbruch
		  	
		  println()
		  	gibt Argumente in der Console wieder mit einem Zeilen umbruch
		*/
		
		//Aufgabe 2 
		System.out.println("               * \n	      ***\n	     *****\n	    *******\n	   *********\n	  ***********\n	 *************\n	      ***\n 	      ***");

		//Aufgabe 3
		System.out.printf("%.2f\n", 22.4234234);
		System.out.printf("%.2f\n", 111.2222 );
		System.out.printf("%.2f\n", 4.0 );
		System.out.printf("%.2f\n", 1000000.551 );
		System.out.printf("%.2f\n", 97.34 );
		

	}

}
