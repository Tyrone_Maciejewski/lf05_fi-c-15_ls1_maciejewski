import java.util.Scanner;

public class Kontrollstrukturen {

    //Aufgabe 1: Eigene Bedingungen 
    public static void main(String[] args) {
        //1.
        //Wenn 7 Uhr dann aufstehen

        //2.
        Scanner tastatur = new Scanner(System.in);
        double zuZahlenderBetrag;
        short ticketMenge;
        System.out.print("Wie sp�t ist es jetzt ?: ");
        int AktuelleUhrzeit = tastatur.nextInt();
        System.out.print("Wann willst du aufstehen ?: ");
        int Aufstehen = tastatur.nextInt();
        if (AktuelleUhrzeit == Aufstehen) {
            System.out.print("DU MUSST AUFSTEHEN !!!");
            PrintTime(Aufstehen);
        }

        //3.
        System.out.print("Wie sp�t ist es jetzt ?: ");
        AktuelleUhrzeit = tastatur.nextInt();
        System.out.print("Wann willst du aufstehen ?: ");
        Aufstehen = tastatur.nextInt();
        if (AktuelleUhrzeit == Aufstehen) {
            System.out.print("DU HAST VERSCHLAFEN");
            PrintTime(Aufstehen);
        }
        //4.
        System.out.print("Wie sp�t ist es jetzt ?: ");
        AktuelleUhrzeit = tastatur.nextInt();
        System.out.print("Wann willst du aufstehen ?: ");
        Aufstehen = tastatur.nextInt();
        if (AktuelleUhrzeit == Aufstehen || Aufstehen > AktuelleUhrzeit) {
            System.out.println("Aufstehen musst du erst um " + Aufstehen + " Uhr");

        } else {
            System.out.print("Du hast verschlafen");
            PrintTime(AktuelleUhrzeit);
        }

        //1.
        System.out.print("Zahl 1: ");
        int zahl1 = tastatur.nextInt();
        System.out.print("Zahl 2: ");
        int zahl2 = tastatur.nextInt();
        System.out.print("Zahl 3: ");
        int zahl3 = tastatur.nextInt();

        if ((zahl1 > zahl2) && (zahl1 > zahl3)) {
            System.out.println(zahl1 + " ist gr��er als " + zahl2 + " und " + zahl3);
        }

        //2.
        System.out.print("Zahl 1: ");
        zahl1 = tastatur.nextInt();
        System.out.print("Zahl 2: ");
        zahl2 = tastatur.nextInt();
        System.out.print("Zahl 3: ");
        zahl3 = tastatur.nextInt();
        if ((zahl3 > zahl1) && (zahl3 > zahl2)) {
            System.out.println(zahl3 + " ist die gr��te Zahl");
        }

        //2.
        System.out.print("Zahl 1: ");
        zahl1 = tastatur.nextInt();
        System.out.print("Zahl 2: ");
        zahl2 = tastatur.nextInt();
        System.out.print("Zahl 3: ");
        zahl3 = tastatur.nextInt();
        if ((zahl3 > zahl1) || (zahl3 == zahl2)) {
            System.out.println(zahl3 + " ist die gr��te Zahl oder die zweite Zahl enspricht der ersten");
        }


        //3.
        System.out.print("Zahl 1: ");
        zahl1 = tastatur.nextInt();
        System.out.print("Zahl 2: ");
        zahl2 = tastatur.nextInt();
        System.out.print("Zahl 3: ");
        zahl3 = tastatur.nextInt();

        if ((zahl1 > zahl2) && (zahl1 > zahl3)) {
            System.out.println("Die gr��te Zahl ist die: " + zahl1);
        } else if ((zahl2 > zahl1) && (zahl2 > zahl3)) {
            System.out.println("Die gr��te Zahl ist die: " + zahl2);
        } else if ((zahl3 > zahl1) && (zahl3 > zahl2)) {
            System.out.println("Die gr��te Zahl ist die: " + zahl3);
        }

        //Aufgabe 2: Steuersatz

		double Brutto;
		double netto;
		
		System.out.print("Netto: ");
		netto = tastatur.nextDouble();
		System.out.print("Wollen sie den erm��igten (j) oder den volle Steuersatz anwenden (n): ");
        Brutto = Steuersatz(netto);     
        System.out.println("Der Bruttowert Betrag: " + Brutto);
        
      //Aufgabe 3: Hardware-Gro�h�ndler
        
        double MausAnzahl;
        double MausPreisSt�ck;
        double Lieferkosten = 10;
        System.out.print("Anzahl der bestellten M�use: ");
        MausAnzahl = tastatur.nextDouble();
        System.out.print("Einzelpreis eingegeben pro Maus: ");
        MausPreisSt�ck = tastatur.nextDouble();
        System.out.print("Sie w�rden " + HardwareCalc(MausAnzahl, MausPreisSt�ck, Lieferkosten) + " Bezahlen");
        
       //Aufgabe 4: Rabattsystem
	
        double Bestellwert;
        
        System.out.print("Bestellwert: ");
        Bestellwert = tastatur.nextDouble();
        System.out.print("Ihr neuer Bestellwert betr�gt : " + Rabattsystem(Bestellwert) + " Euro");
       
    }
    public static double Rabattsystem (double Bestellwert) {
    	double Rabatt = 0;
    	double Berechnung = 0;
    	
    	if (Bestellwert  >= 0 && Bestellwert <= 100 ) {
    		Rabatt = 10;
    		Berechnung = Bestellwert - (Bestellwert / 100 * Rabatt ) ;
    	} else if (Bestellwert  >= 100 && Bestellwert <= 500 ) {
    		Rabatt = 15;
    		Berechnung = Bestellwert - (Bestellwert / 100 * Rabatt ) ;
    	}  else  {
    		Rabatt = 20;
    		Berechnung = Bestellwert - (Bestellwert / 100 * Rabatt ) ;
    	}
		return Berechnung;
    }
		
    public static double HardwareCalc(double MausAnzahl,  double MausPreisSt�ck, double Lieferkosten) {

    	double LieferkostenPreis = 0; 
    	double Berechnung = 0;
    	if (MausAnzahl < 10) {
    		LieferkostenPreis = Lieferkosten; 
    		Berechnung =  MausAnzahl * MausPreisSt�ck + LieferkostenPreis;
    	} else if ( MausAnzahl > 10) {
    		LieferkostenPreis = 0;
    		Berechnung =  MausAnzahl * MausPreisSt�ck + LieferkostenPreis;
    	}
    	
		return Berechnung;
    }
    

    public static double Steuersatz(double netto) {
    	Scanner tastatur = new Scanner(System.in);
        double brutto = 0;
        String Abfrage;
        int percent = 0;
        
        Abfrage = tastatur.nextLine();
        if (Abfrage.equals("ja") || (Abfrage.equals("j"))) {
        	percent = 7;
        	brutto =  netto / 100 * percent;
        } else  if (Abfrage.equals("nein") || (Abfrage.equals("n"))) {
        	percent = 19;
        	brutto =  netto / 100 * percent;
        }
        return  netto + brutto  ;        
	}
    
    public static void PrintTime(int time) {
        System.out.println(" es ist " + time + " Uhr");
    }



}