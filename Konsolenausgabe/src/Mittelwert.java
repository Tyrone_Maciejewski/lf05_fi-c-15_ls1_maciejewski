import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
	    
	   Scanner Scanner = new Scanner(System.in);
	   System.out.println("Geben sie eine Zahl ein: ");
      double x = Scanner.nextDouble();
      System.out.println("Geben sie eine Zahl ein: ");
      double y = Scanner.nextDouble();
      double m;
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
    
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      m = Mittelwert(x,y);
      
      Methode(x,y,m);
  
   }
   public static void Mittelwert(double x, double y){
	  return (x + y) / 2.0;

   }
   
   public static void Methode(double x, double y, double m) {
	      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
}
