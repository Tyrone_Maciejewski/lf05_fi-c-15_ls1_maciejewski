﻿import java.util.Scanner;

class Fahrkartenautomat
{

	  public static void main(String[] args) {
		  fahrkartenbestellungErfassen();
	  }
	  
	 public static void warte(int millisekunde)
	 {
		  try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
		}
	  }
	 
    public static void fahrkartenbestellungErfassen()
    { 
    	 Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag; 
    	short ticketMenge;
       System.out.print("Ticketpreis (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();

       System.out.print("Anzahl der Tickets (1 - 10): ");
       ticketMenge = tastatur.nextShort();
       
       if (ticketMenge > 0 && ticketMenge < 11) {  	   
           zuZahlenderBetrag = ticketMenge * zuZahlenderBetrag;       
           fahrkartenBezahlen(zuZahlenderBetrag);   
       }else { 	   
    	   System.out.println("FEHLER : Deine Tickezanzahl ist außerhalb des verfügbaren bereiches, fahre mit 1 Ticket fort.");
    	   ticketMenge = 1;
    	   zuZahlenderBetrag = ticketMenge * zuZahlenderBetrag;       
           fahrkartenBezahlen(zuZahlenderBetrag);   
       }

    }
       
    public static void fahrkartenBezahlen(double zuZahlenderBetrag) {
    	 Scanner tastatur = new Scanner(System.in);
    	 double eingezahlterGesamtbetrag;
    	  double eingeworfeneMünze;
    	 
    	 eingezahlterGesamtbetrag = 0.0;
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   
     	   System.out.print("Noch zu zahlen: ");
     	   System.out.printf("%.2f\n", zuZahlenderBetrag - eingezahlterGesamtbetrag);
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	fahrkartenAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
    }
    
       public static void fahrkartenAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag )
       {
    	    Scanner tastatur = new Scanner(System.in);
  
       System.out.println("\nFahrschein wird ausgegeben");
       
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          warte(250);
       }
       System.out.println("\n\n");
       rueckgeldAusgeben(eingezahlterGesamtbetrag,zuZahlenderBetrag);
       }
    
    public static void rueckgeldAusgeben( double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	 double rückgabebetrag;   

       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.print("Der Rückgabebetrag in Höhe von ");
    	   System.out.printf("%.2f", rückgabebetrag);
    	   System.out.println(" EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {

        	  muenzeAusgeben(2,"EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	   muenzeAusgeben(1,"EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	   muenzeAusgeben(50,"CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	   muenzeAusgeben(20,"CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	   muenzeAusgeben(10,"CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	   muenzeAusgeben(5,"CENT");
 	          rückgabebetrag -= 0.05;
           }
       }
       
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
    public static void muenzeAusgeben(int betrag, String einheit) {
    	  System.out.println(betrag + " " + einheit);
    }
}

// 5. Bei der Variable ticketMenge habe ich mich für den Datentyp Short entschieden.
// Dieser ist ideal für uns  da keiner eine Ticket Menge von über -32 768 bis 32 768 kaufen wird.


// 6. Die Variable zuZahlenderBetrag bekommt das Ergebnis der Multiplikation von ticketMenge * zuZahlenderBetrag;
// Die Variable zuZahlenderBetrag wird so oft multipliziert wie die ticketMenge sagt.